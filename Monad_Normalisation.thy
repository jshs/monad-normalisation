(*  Title:      Monad_Normalisation.thy
    Author:     Manuel Eberl, TU München
    Author:     Joshua Schneider, ETH Zurich

Normalisation of monadic expressions.
*)

section \<open>Normalisation of monadic expressions\<close>

theory Monad_Normalisation
imports
  Probability
  SPMF
  "~~/src/HOL/Library/Monad_Syntax"
begin

subsection \<open>Registration of the monads from the library\<close>

ML_file "monad_rules.ML"


lemmas [monad_rule] = Set.bind_bind

lemma set_bind_commute [monad_rule]:
  fixes A :: "'a set" and B :: "'b set"
  shows "A \<bind> (\<lambda>x. B \<bind> C x) = B \<bind> (\<lambda>y. A \<bind> (\<lambda>x. C x y))"
  unfolding Set.bind_def by auto

lemma set_return_bind [monad_rule]:
  fixes A :: "'a \<Rightarrow> 'b set"
  shows "{x} \<bind> A = A x"
  unfolding Set.bind_def by auto

lemma set_bind_return [monad_rule]:
  fixes A :: "'a set"
  shows "A \<bind> (\<lambda>x. {x}) = A"
  unfolding Set.bind_def by auto


lemmas [monad_rule] = Predicate.bind_bind Predicate.bind_single Predicate.single_bind

lemma predicate_bind_commute [monad_rule]:
  fixes A :: "'a Predicate.pred" and B :: "'b Predicate.pred"
  shows "A \<bind> (\<lambda>x. B \<bind> C x) = B \<bind> (\<lambda>y. A \<bind> (\<lambda>x. C x y))"
  by (intro pred_eqI) auto


lemmas [monad_rule] = Option.bind_assoc Option.bind_lunit Option.bind_runit

lemma option_bind_commute [monad_rule]:
  fixes A :: "'a option" and B :: "'b option"
  shows "A \<bind> (\<lambda>x. B \<bind> C x) = B \<bind> (\<lambda>y. A \<bind> (\<lambda>x. C x y))"
  by (cases A) auto


lemmas [monad_rule] =
  bind_assoc_pmf
  bind_commute_pmf
  bind_return_pmf
  bind_return_pmf'

lemmas [monad_rule] =
  bind_spmf_assoc
  bind_commute_spmf
  bind_return_spmf
  return_bind_spmf


subsection \<open>Distributive operators\<close>

lemma bind_if [monad_distrib]:
  "f A (\<lambda>x. if P then B x else C x) = (if P then f A B else f A C)"
by simp

lemma bind_case_prod [monad_distrib]:
  "f A (\<lambda>x. case y of (a,b) \<Rightarrow> B a b x) = (case y of (a,b) \<Rightarrow> f A (B a b))"
by (simp split: prod.split)

lemma bind_case_sum [monad_distrib]:
  "f A (\<lambda>x. case y of Inl a \<Rightarrow> B a x | Inr a \<Rightarrow> C a x) =
    (case y of Inl a \<Rightarrow> f A (B a) | Inr a \<Rightarrow> f A (C a))"
by (simp split: sum.split)

lemma bind_case_option [monad_distrib]:
  "f A (\<lambda>x. case y of Some a \<Rightarrow> B a x | None \<Rightarrow> C x) =
    (case y of Some a \<Rightarrow> f A (B a) | None \<Rightarrow> f A C)"
by (simp split: option.split)


subsection \<open>Setup of the normalisation procedure\<close>

ML_file "monad_normalisation.ML"

simproc_setup monad_normalisation ("f x y") = \<open>K Monad_Normalisation.normalise_step\<close>
declare [[simproc del: monad_normalisation]]

text \<open>The following bundle enables normalisation of monadic expressions by the simplifier.\<close>

bundle monad_normalisation = [[simproc add: monad_normalisation]] monad_simps[simp]


end